// This file contain only tests

#include "ClassHook.h"

namespace {
	// Example of virtual class
	class Test {
	public:
		virtual ~Test() {
		};

		virtual void foo() {
		};

		virtual void bar() {
		};
	};

	// Virtual table of class Test
	struct TestVtbl {
#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32 ) || defined( __WIN32__ ) ) && !defined( WIN64 ) && !defined( _WIN64 ) && !defined( __WIN64 ) && !defined( __WIN64__ )
		void (__thiscall *dtor)( Test *self ) = nullptr;
		void (__thiscall *dtor_with_delete)( Test *self ) = nullptr; // NOTE: does not exists for MSVC compiler
		void (__thiscall *foo)( Test *self ) = nullptr;
		void (__thiscall *bar)( Test *self ) = nullptr;
#else
		void (*dtor)( Test *self ) = nullptr;
		void (*dtor_with_delete)( Test *self ) = nullptr;
		void (*foo)( Test *self ) = nullptr;
		void (*bar)( Test *self ) = nullptr;
#endif
	};

	struct TestVtblBad {
		virtual void a() {
		};
	};

	struct TestVtblBad2 {
		int a;
		float b;
		char c;
	};

	// Testing valid types for vtbl
	static_assert( is_valid_vtbl<TestVtbl>::value );
	static_assert( !is_valid_vtbl<TestVtblBad>::value );
	static_assert( !is_valid_vtbl<TestVtblBad2>::value );
	static_assert( !is_valid_vtbl<int>::value );
	// Testing count of methods in vtbl
	static_assert( ClassHook<TestVtbl>::size() == 4 );
	// Testing size of vtbl
	static_assert( sizeof( ClassHook<TestVtbl>::vtbl ) == sizeof( TestVtbl ) );
	// Testing remove properties
	static_assert( std::is_same<ClassHook<TestVtbl *>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<TestVtbl &>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<const TestVtbl *>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<const TestVtbl &>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<volatile TestVtbl *>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<volatile TestVtbl &>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<const volatile TestVtbl *>::type, TestVtbl>::value );
	static_assert( std::is_same<ClassHook<const volatile TestVtbl &>::type, TestVtbl>::value );

	// Testing non-constexpr actions
	void test() {
		Test obj;
		auto hook = ClassHook<TestVtbl>( (const Test *)&obj );
		hook.vtbl.methods.foo = hook.vtbl.methods.bar;
		for ( auto &ptr: hook.vtbl.ptrs )
			ptr = nullptr;
	}
}
