#pragma once

#include <cstring>
#include <type_traits>

namespace details {
	template <typename T>
	struct remove_all {
	protected:
		using _type_no_ptr = typename std::remove_pointer<T>::type;
		using _type_no_ref = typename std::remove_reference<_type_no_ptr>::type;
	public:
		using type = typename std::remove_cv<_type_no_ref>::type;
	};
	template <typename T>
	struct is_pod {
	protected:
		static constexpr auto _is_standard = std::is_standard_layout<T>::value;
		static constexpr auto _is_trivial = std::is_trivial<T>::value;
	public:
		static constexpr auto value = _is_standard && _is_trivial;
	};
}

/**
 * @brief Check is type T is valid vtbl struct
 * @tparam T vtbl struct
 */
template <typename T>
struct is_valid_vtbl {
protected:
	static constexpr auto _clazz = std::is_class<T>::value;
	static constexpr auto _pod = details::is_pod<T>::value;
	static constexpr auto _polymorphic = std::is_polymorphic<T>::value;
public:
	static constexpr auto value = _clazz && !_pod && !_polymorphic;
};

/**
 * @brief Hook virtual table
 * @tparam VirtualMethods list of virtual methods
 */
template <class VirtualMethods>
class ClassHook {
public:
	using type = typename details::remove_all<VirtualMethods>::type;
	static_assert( is_valid_vtbl<type>::value, "ClassHook: Vtbl type must contain only function ptrs" );

	static constexpr auto kVirtualMethodsCount = sizeof( type ) / sizeof( void * );

	/**
	 * @brief Create hook for object
	 * @tparam ClassName Type of hooked object
	 * @param object Pointer to object
	 */
	template <class ClassName>
	explicit ClassHook( ClassName *object ) {
#if __cplusplus > 201700L
		if constexpr ( std::is_const<ClassName>::value || std::is_volatile<ClassName>::value ) {
			using type_no_cv = typename std::remove_cv<ClassName>::type;
			auto object_no_cv = const_cast<type_no_cv*>(object);
			obj_vtbl = reinterpret_cast<type **>(object_no_cv);
		} else
			obj_vtbl = reinterpret_cast<type **>(object);
#else
		obj_vtbl = (type **)object;
#endif
		static_assert( std::is_class<ClassName>::value, "ClassHook: Invalid object type" );
		orig_vtbl = *obj_vtbl;
		std::memcpy( &vtbl.methods, orig_vtbl, sizeof( type ) );
		*obj_vtbl = &vtbl.methods;
	}

	~ClassHook() {
		*obj_vtbl = orig_vtbl;
	}

	/// new vtbl used by class
	union {
		type methods{};
		void *ptrs[kVirtualMethodsCount];
	} vtbl;

	/**
	 * @brief Getting count of methods in virtual table
	 * @return count
	 */
	static constexpr std::size_t size() {
		return kVirtualMethodsCount;
	}

protected:
	type *orig_vtbl;
	type **obj_vtbl;
};
